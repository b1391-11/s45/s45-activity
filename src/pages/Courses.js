
import courseData from "./../data/courseData"

/*component*/
import CourseCard from "./../components/CourseCard"

function Courses() {
    console.log(courseData)
	const courses = courseData.map(course => {
		return (
			<CourseCard key={course.id} courseProp={course}/>
		)
	});

	return(
		<>
			{courses}
		</>	
	)

};

export default Courses;
