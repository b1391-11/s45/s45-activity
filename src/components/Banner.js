

export default function Banner() {


	return(
		<div className="container-fluid mt-5">
			<div className="row justify-content-center">
				<div className="col-10 col-md-8">
					{/*jumbotron*/}
					<div className="jumbotron">
						<h1>Welcome to React-Booking</h1>
						<p>Opportunities for everyone, everywhere!</p>
						<button className="btn btn-primary">Enroll</button>
					</div>
				</div>
			</div>
		</div>
	)
}