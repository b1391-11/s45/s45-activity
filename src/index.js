import React from 'react';
import ReactDOM from 'react-dom';


import 'bootstrap/dist/css/bootstrap.min.css';

import App from './App';


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);



//Mini Activity
//Create another variable that is object which contains properties firstName and LastName
//using a function, join the firstName and lastName and display it in the browser
//Send screenchat in the group hangouts

/*const user = {
	firstName: "Amiel",
	lastName: "Buenaventura"
}

function fullName(user){
	return `${user.firstName} ${user.lastName}`
}

const element = <h1>Hello, {fullName(user)}</h1>

ReactDOM.render(
  element,
  document.getElementById('root')
);*/

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

