import {Container, Row, Col, Card} from "react-bootstrap"


export default function Highlights(){

	return(
		/*card 1*/
		<Container fluid className= "mb-4">
			<Row>
				<Col xs={12} md={4}>
					{/*card 1*/}
					<Card className="cardHighlights p-3">
					  <Card.Body>
					    <Card.Title>Learn from Home</Card.Title>
					    <Card.Text>
					      Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio, architecto, possimus iure odit, quas expedita perspiciatis velit explicabo esse sequi non illum assumenda quibusdam laborum. Vitae nemo voluptas, expedita voluptatibus.
					    </Card.Text>
					  </Card.Body>
					</Card>	
				</Col>
			{/*card 2*/}
				<Col xs={12} md={4}>
					{/*card 1*/}
					<Card className="cardHighlights p-3">
					  <Card.Body>
					    <Card.Title>Study Now, Pay Later</Card.Title>
					    <Card.Text>
					      Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio, architecto, possimus iure odit, quas expedita perspiciatis velit explicabo esse sequi non illum assumenda quibusdam laborum. Vitae nemo voluptas, expedita voluptatibus.
					    </Card.Text>
					  </Card.Body>
					</Card>	
				</Col>
				{/*card 13*/}
				<Col xs={12} md={4}>
					{/*card 3*/}
					<Card className="cardHighlights p-3">
					  <Card.Body>
					    <Card.Title>Be Part of our Community</Card.Title>
					    <Card.Text>
					      Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio, architecto, possimus iure odit, quas expedita perspiciatis velit explicabo esse sequi non illum assumenda quibusdam laborum. Vitae nemo voluptas, expedita voluptatibus.
					    </Card.Text>
					  </Card.Body>
					</Card>	
				</Col>
			</Row>
		</Container>
	)

}