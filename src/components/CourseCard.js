import { useState } from 'react';
import {Container, Row, Col, Card, Button} from "react-bootstrap";
import PropTypes from 'prop-types';

export default function CourseCard({courseProp}){
	console.log(courseProp)

	const {name, description, price} = courseProp

	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(10);


	function enroll() {
			if (seats > 0) {
				setCount(count + 1);
				setSeats(seats - 1);
			} else {
				alert('No more seats!');
			}
		}


	return(
		<Container fluid className="mb-4">
			<Row className="justify-content-center">
				<Col xs={10} md={8}>
					<Card className="p-4">
					  <Card.Title>{name}</Card.Title>
					  <Card.Body>
					  	<Card.Text>Description:</Card.Text>
					  	<Card.Text>{description}</Card.Text>
					  	<Card.Text>Price:</Card.Text>
					  	<Card.Text>{price}</Card.Text>
					  	<Card.Text>Enrollees: {count}</Card.Text>
					  	<Button variant="primary" onClick={enroll}>Enroll</Button>
					  </Card.Body>
					</Card>
				</Col>
			</Row>		
		</Container>
	)
}

CourseCard.propTypes = {
	courseProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
